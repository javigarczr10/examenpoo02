/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenpoo02;

/**
 *
 * @author safoe
 */
public class Examenpoo02 {
private int numDocente;
private String nombre;
private String domicilio;
private int nivel;
private float pagos;
private float horas;
    

public Examenpoo02(){
    this.numDocente=0;
    this.nombre="";
    this.domicilio="";
    this.nivel=0;
    this.pagos=0.0f;
    this.horas=0.0f;
}

    public Examenpoo02(int numDocente, String nombre, String domicilio, int nivel, float pagos, float horas) {
        this.numDocente = numDocente;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.pagos = pagos;
        this.horas = horas;
    }

public Examenpoo02(Examenpoo02 otro){
    this.numDocente=otro.numDocente;
    this.nombre=otro.nombre;
    this.domicilio=otro.domicilio;
    this.nivel=otro.nivel;
    this.pagos=otro.pagos;
    this.horas=otro.horas;
}

    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getPagos() {
        return pagos;
    }

    public void setPagos(float pagos) {
        this.pagos = pagos;
    }

    public float getHoras() {
        return horas;
    }

    public void setHoras(float horas) {
        this.horas = horas;
    }

public float CalcularPago(){
    float pagoFinal=0;
    if (this.nivel == 0) {
        pagoFinal = this.pagos * 1.30f; 
    } else if (this.nivel == 1) {
        pagoFinal = this.pagos * 1.50f; 
    } else if (this.nivel == 2) {
        pagoFinal = this.pagos * 2.0f; 
    }
    return pagoFinal*this.horas;
}  

public float CalcularBono(int Hijos){
float Bono=0.0f;
float PagTo=CalcularPago();
if(Hijos>=1&&Hijos<=2){
    Bono = PagTo * 0.05f;
}
if(Hijos>=3&&Hijos<=5){
    Bono = PagTo * 0.10f;
}
if(Hijos>5){
    Bono = PagTo * 0.20f;
}
return Bono;
    
}

public float CalculoTotal(int Hijos){

    float total=CalcularPago()-CalcularImpuesto()+CalcularBono(Hijos);
    return total;
}

public float CalcularImpuesto(){
    return CalcularPago()*0.16f;
}

}
